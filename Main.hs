-- заменяет елемент в матрице matrix в позиции posX posY на element
replaceElementInMatrix :: Int -> Int -> a -> [[a]] -> [[a]]
replaceElementInMatrix posX posY element matrix =
                       let (a, b) = splitAt posY matrix
                           (a1, a2) = splitAt (posY - 1) a
                           a3 = (!!) a2 0
                           a4 = replaceElement posX element a3
                       in a1 ++ [a4] ++ b

-- заменяет элемент в списке array в позиции pos на el
replaceElement :: Int -> a -> [a] -> [a]
replaceElement pos el array = 
	           let (tempPart,part2) = splitAt pos array
                       part1 = init tempPart ++ [el]
               in part1 ++ part2

-- создает матрицу с размером стороны size, заполненную элементом element
createMatrix :: Integer -> a -> [[a]]
createMatrix size element = replicate s (replicate s element)
                            where s = fromInteger size

getMatrix :: Integer -> [[Int]]
getMatrix size = doTop matrix size 1 1 1
                 where matrix = createMatrix size 0

-- заменяет в матрице элементы из списка valList с координатами xList yList
repl :: [Int] -> [Int] -> [a] -> [[a]] -> [[a]]
repl [] [] [] a = a
repl xList yList valList matrix = let x = head xList
                                      y = head yList
                                      v = head valList
                                      newMatrix = replaceElementInMatrix x y v matrix
                                      newXList = tail xList
                                      newYList = tail yList
                                      newValList = tail valList
                                  in repl newXList newYList newValList newMatrix

doTop :: [[Int]] -> Integer -> Integer -> Integer -> Integer -> [[Int]]
doTop matrix size x y number
    | number > size * size = matrix
    | otherwise = let start = x
                      finish = (size + 1) - x
                      xList = map fromInteger [start .. finish]
                      diap = fromInteger (finish - start) + 1
                      yList = map fromInteger ( replicate diap y)
                      numbers = map fromInteger [number  .. number + toInteger diap - 1]
                      newMatrix = repl xList yList numbers matrix
                      newX = finish
                      newY = y + 1
                      newNumber = number + toInteger diap
                   in doRight newMatrix size newX newY newNumber

doRight :: [[Int]] -> Integer -> Integer -> Integer -> Integer -> [[Int]]
doRight matrix size x y number
    | number > size * size = matrix
    | otherwise = let start = y
                      finish = x
                      diap = fromInteger (finish - start) + 1
                      xList = map fromInteger (replicate diap x)
                      yList = map fromInteger [start .. finish]
                      numbers = map fromInteger [number  .. number + toInteger diap - 1]
                      newMatrix = repl xList yList numbers matrix
                      newX = x - 1
                      newY = finish
                      newNumber = number + toInteger diap
                   in doBottom newMatrix size newX newY newNumber

doBottom :: [[Int]] -> Integer -> Integer -> Integer -> Integer -> [[Int]]
doBottom matrix size x y number
    | number > size * size = matrix
    | otherwise = let start = x
                      finish = size - x
                      diap = fromInteger (start - finish) + 1
                      xList = reverse $ map fromInteger [finish .. start]
                      yList = map fromInteger (replicate diap y)
                      numbers = map fromInteger [number .. number + toInteger diap - 1]
                      newMatrix = repl xList yList numbers matrix
                      newX = finish
                      newY = y - 1
                      newNumber = number + toInteger diap
                  in doLeft newMatrix size newX newY newNumber

doLeft :: [[Int]] -> Integer -> Integer -> Integer -> Integer -> [[Int]]
doLeft matrix size x y number
    | number > size * size = matrix
    | otherwise = let start = y
                      finish = x + 1
                      diap = fromInteger (start - finish) + 1
                      xList = map fromInteger (replicate diap x)
                      yList = reverse $ map fromInteger [finish .. start]
                      numbers = map fromInteger [number .. number + toInteger diap - 1]
                      newMatrix = repl xList yList numbers matrix
                      newX = x + 1
                      newY = finish
                      newNumber = number + toInteger diap
                   in doTop newMatrix size newX newY newNumber

serializeList :: [Int] -> String
serializeList [] = ""
serializeList l = let firstEl = head l
                      t = tail l
                      res = serializeList t
                  in show firstEl ++ "\t" ++ res

serializeMatrix :: [[Int]] -> String
serializeMatrix matrix
        | [] == head matrix = ""
        | otherwise = let fEl = head matrix
                          toShow = serializeList fEl
                          t = tail matrix
                          res = serializeMatrix t
                      in toShow ++ "\n" ++ res

main = putStrLn $ serializeMatrix $ getMatrix 20