<?php

function getMatrix($size)
{
    $matrix = array();

    for ($i = 0; $i < $size; $i++) {
        $matrix[] = array_fill(0, $size, 0);
    }

    doTop($matrix, $size, 1, 1, 1);

    return $matrix;
}

function printMatrix($matrix)
{
    foreach ($matrix as $row) {
        foreach ($row as $field) {
            echo str_pad($field, 4, ' ');
        }
        echo "\n";
    }
}

function doTop(&$matrix, $size, $x, $y, $number)
{
    if ($number > $size * $size) {
        return;
    }

    $start = $x;
    $finish = ($size + 1) - $x;

    for ($i = $start; $i <= $finish; $i++) {
        $matrix[$y - 1][$i - 1] = $number++;
    }

    $x = $finish;
    $y += 1;

    doRight($matrix, $size, $x, $y, $number);
}

function doRight(&$matrix, $size, $x, $y, $number)
{
    if ($number > $size * $size) {
        return;
    }

    $start = $y;
    $finish = $x;

    for ($i = $start; $i <= $finish; $i++) {
        $matrix[$i - 1][$x - 1] = $number++;
    }

    $x -= 1;
    $y = $finish;

    doBottom($matrix, $size, $x, $y, $number);
}

function doBottom(&$matrix, $size, $x, $y, $number)
{
    if ($number > $size * $size) {
        return;
    }

    $start = $x;
    $finish = $size - $x;

    for ($i = $start; $i >= $finish; $i--) {
        $matrix[$y - 1][$i - 1] = $number++;
    }

    $x = $finish;
    $y -= 1;

    doLeft($matrix, $size, $x, $y, $number);
}

function doLeft(&$matrix, $size, $x, $y, $number)
{
    if ($number > $size * $size) {
        return;
    }

    $start = $y;
    $finish = $x + 1;

    for ($i = $start; $i >= $finish; $i--) {
        $matrix[$i - 1][$x - 1] = $number++;
    }

    $x += 1;
    $y = $finish;

    doTop($matrix, $size, $x, $y, $number);
}

$matrix = getMatrix(10);
printMatrix($matrix);
